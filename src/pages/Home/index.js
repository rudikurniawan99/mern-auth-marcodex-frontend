import { Box, Container, Grid, Typography } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import { DummyProductPhoto } from '../../assets/images'
import { Product as ProductItem } from '../../components/atoms'
import { Header } from '../../components/molecules'
import axios from 'axios'

const Home = () => {
  const [products, setProducts] = useState([])

  const getProducts = () => {
    axios.get('http://127.0.0.1:4000/api/v1/product/products?page=1&itemLimit=8')
      .then((result) => {
        setProducts(result.data.data)
        // console.log(result.data.data)
        console.log(products)
      })
  }

  useEffect(() => {
    getProducts()
  }, [])

  return (
    <div>
      <Header />
      <Container>
        <Box
          py={5} 
        >
          <Typography
           gutterBottom
           variant="subtitle2"
          >
            Daftar Produk
          </Typography>
          <Grid 
            container
            spacing={4}
          >
            { products.map((product) => {
              return (

                <Grid
                  item 
                  xs={12}
                  md={4}
                  lg={3}
                >
                  <ProductItem
                    product={product}
                    imagePath={DummyProductPhoto} 
                  >
                  </ProductItem>
                </Grid>
              )
            }) }
            
          </Grid>
        </Box>
      </Container>
       
    </div>
  )
}

export default Home
