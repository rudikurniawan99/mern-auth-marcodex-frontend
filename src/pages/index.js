import Home from './Home'
import Register from './Auth/Register'
import Login from './Auth/Login'

export { Home, Register, Login }