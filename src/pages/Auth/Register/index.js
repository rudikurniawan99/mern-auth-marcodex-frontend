import { makeStyles } from '@material-ui/styles'
import { Container, Box, Grid, TextField, Button, Card, CardActionArea, CardContent } from '@material-ui/core'
import React from 'react'

const useStyle = makeStyles((theme) => ({
  authCard: {
    // maxWidth: '25%'
  },
  formControl: {
    width: '100%',
    color: '#9e9e9e',
    marginBottom: 20
  }
}))

const Register = () => {

  const classess = useStyle()

  return (
    <div>
      <Container>
        <Box 
          py={5}
          justifyItems="center"
        >
          <Grid 
            container
            direction="row"
            justifyContent="center"
          >
            <Grid
              item 
              xs={12}
              lg={4}
            >
              <Card>
                <CardActionArea>
                  <CardContent>
                    <Box
                     p={3} 
                     component="form"
                    >
                      <TextField className={classess.formControl} id="fullname" label="Fullname" variant="outlined" />
                      <TextField className={classess.formControl} id="email" label="Email" variant="outlined" />
                      <TextField className={classess.formControl} id="password" label="Password" variant="outlined" />
                      <Button
                        fullWidth
                        variant="contained"  
                        color="primary"
                      >
                        Simpan
                      </Button>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </div>
  )
}

export default Register
