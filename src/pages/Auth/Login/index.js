import { Box, Button, Card, CardActionArea, CardContent, Container, Grid, makeStyles, Paper, TextField, Typography } from '@material-ui/core'
import React from 'react'
import { Link } from 'react-router-dom'

const useStyle = makeStyles((theme) => ({
  authCard: {
    // maxWidth: '25%'
  },
  formControl: {
    width: '100%',
    color: '#9e9e9e',
    marginBottom: 20
  },
  registerRedirect: {
    textDecoration: 'none'
  }
}))
const Login = () => {

  const classess = useStyle()

  return (
    <div>
      <Container>
        <Box 
          py={5}
          justifyItems="center"
        >
          <Grid 
            container
            direction="row"
            justifyContent="center"
          >
            <Grid
              item 
              xs={12}
              lg={4}
            >
              <Card>
                <CardActionArea>
                  <CardContent>
                    <Box
                      px={2}
                      py={5}
                      component="form"
                    >
                      <TextField className={classess.formControl} label="email" variant="outlined" />
                      <TextField className={classess.formControl} label="password" variant="outlined" />
                      <Button
                        fullWidth
                        variant="contained"  
                        color="primary"
                      >
                        Login
                      </Button>
                    </Box>
                    <Box
                      display="flex" 
                      justifyContent="center"
                    >
                      <Link
                        to='/register'
                        className={classess.registerRedirect}
                      >
                        <Typography
                          variant="subtitle2" 
                        >
                          Don't have account?, register here
                        </Typography>
                      </Link>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </div>
  )
}

export default Login
