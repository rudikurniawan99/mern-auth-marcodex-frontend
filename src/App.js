import { CssBaseline } from "@material-ui/core";
import Routes from "./config/Routes";

function App() {
  return (
    <>
      <CssBaseline/>
      <Routes/>
    </> 
  );
}

export default App;
