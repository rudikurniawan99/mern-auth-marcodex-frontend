import { Box, Card, CardActionArea, CardContent, CardMedia, makeStyles, Typography } from '@material-ui/core'
import React from 'react'

const useStyles = makeStyles((theme) => ({
  media: {
    height: 175
  }
}))

const Product = ({product, imagePath}) => {
  const classess = useStyles()

  const formatter = (number) => {
    return new Intl.NumberFormat('id-ID').format(number)
  } 

  return (
    <Card>
      <CardActionArea>
        <CardMedia
          className={classess.media}
          title={product.name} 
          image={`http://127.0.0.1:4000/${product.thumbnail}`}
          
        />
        <CardContent>
          <Typography
          >
            {product.name}
          </Typography>
          <Typography
            variant="caption" 
            color="secondary"
          >
            Rp { formatter(product.price) }
          </Typography>
          
          <Typography
            variant="body2" 
          >
            {product.description}
          </Typography>

        </CardContent>
      </CardActionArea>
    </Card>
  )
}

export default Product
