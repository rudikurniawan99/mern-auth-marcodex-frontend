import { AppBar, makeStyles, Toolbar, Typography, Container, InputBase, Paper, Box, Button, IconButton } from '@material-ui/core'
import React from 'react'
import { Link } from 'react-router-dom'
import SearchIcon from '@material-ui/icons/Search'
import AccountCircleIcon from '@material-ui/icons/AccountCircle'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'

const useStyles = makeStyles((theme)=>({
  root: {
    flexGrow: 1 
  },
  
  navBrand: {
    textDecoration: 'none',
    color: '#2f2f2f'
  },
  navLink: {
    marginLeft: 2,
    [theme.breakpoints.up('md')]: {
      marginLeft: 50
    }
    
  },
  searchField:{
    outline: 'none',
    border: 'none',
    color: '#9e9e9e',
    borderStyle: 'none'

  },
  searchIcon: {
    color: '#9e9e9e'
  }
}))

const Header = () => {
  const classess = useStyles()

  return (
    <div>
      <AppBar
        color="transparent" 
        position="static"
      >
        <Toolbar>
          <Container>
            <Box
              display="flex"
              alignItems="center"
            >
              <Box flexGrow={1}>
                <Link to="/" className={classess.navBrand}>
                  <Typography variant="h6">
                    RK - Store
                  </Typography>
                </Link>
              </Box>
              <Box className={classess.navLink}>
                <Paper>
                  <Box
                    px={1}
                    py={0.5}
                    display="flex"
                    alignItems="center"
                  >
                    <InputBase className={classess.searchField} />
                    <SearchIcon className={classess.searchIcon} />
                  </Box>
                </Paper>
              </Box>
              <Box className={classess.navLink}>
                <IconButton
                >
                  <ShoppingCartIcon/>
                </IconButton>
              </Box>
              <Box className={classess.navLink}>
                <Link>
                  <Button
                    startIcon={<AccountCircleIcon/>}
                  >
                    Login
                  </Button>
                </Link>
              </Box> 
              

            </Box>
        </Container> 
     </Toolbar> 
     </AppBar>
    </div>
  )
}

export default Header
